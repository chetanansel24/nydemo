# NYDemo
# nytime(NewYork Times API )sdemo
Ny Times Most popular API Demo App.
App follows MVVM Architecture, Clean Architecture, latest android components and Used Kotlin as programming language.
For network connection: retrfot lib is used.
For DI: Dagger2 is used.
Coroutine concurrency design pattern is also used.

For Unit Testing : Mockk Lib is used, Mockk repository, dagger2, retrofit lib etc.

How to Run :
Import the code to Andorid studio.
Run and install on the device.

App features:
List of Most popular Article
Top right menu to select Periods like : 1,7,10



